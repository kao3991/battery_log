use warnings;
use strict;
use Acpi::Class;
use Data::Dumper;
use DBI;
my $dsn = 'dbi:SQLite:dbname=battery.db';
my $dbh = DBI -> connect($dsn, '','');
my $acpi_client = Acpi::Class -> new(class => 'power_supply', device => 'BAT0');
my $values = $acpi_client->g_values;
print Dumper $values;
#print $values->{'voltage_now'};
my $stmt = $dbh->prepare('INSERT INTO battery_log (
    voltage, 
    energy, 
    status, 
    alarm, 
    present, 
    percent
) VALUES (
    ?, 
    ?,
    ?, 
    ?,
    ?,
    ?
)');
$stmt -> bind_param(1, $values->{'voltage_now'});
$stmt -> bind_param(2, $values->{'energy_now'});
$stmt -> bind_param(3, $values->{'status'});
$stmt -> bind_param(4, ($values->{'power_now'}<$values->{'alarm'}));
$stmt -> bind_param(5, $values->{'present'});
$stmt -> bind_param(6, $values->{'energy_now'}/$values->{'energy_full'}*100);
$stmt -> execute();
#$stmt -> execute();
#while (my $row = $stmt -> fetchrow_hashref()) {
#    print Dumper $row;
#}
